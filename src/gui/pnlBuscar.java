package gui;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class pnlBuscar extends JPanel {
	
	private Evento evento;
	private JPanel tbpnBuscarNombre ;
	private JPanel tbpnBuscarApellido;
	private JPanel tbpnBuscarCiudad ;
	private JTextField txtBuscarNombre;
	private JTextField txtBuscarApellido;
	private JTextField txtBuscarCiudad;
	private JButton btnBuscar;
	private JButton btnCancelar;

	/**
	 * Create the panel.
	 */
	public pnlBuscar(Evento e) {

		setLayout(null);
		
		this.evento=e;
		
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(10, 11, 463, 258);
		add(layeredPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 11, 453, 247);
		layeredPane.add(tabbedPane);
		
		tbpnBuscarNombre = new JPanel();
		tbpnBuscarNombre.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		tbpnBuscarNombre.setToolTipText("Aca podra buscar por nombre de contacto");
		tbpnBuscarNombre.setLayout(null);
		tabbedPane.addTab("Nombre", null, tbpnBuscarNombre, "Aca podra buscar por nombre de contacto");
		
		JLabel lblNombreABuscar = new JLabel("Nombre a buscar");
		lblNombreABuscar.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNombreABuscar.setBounds(76, 38, 124, 20);
		tbpnBuscarNombre.add(lblNombreABuscar);
		
		txtBuscarNombre = new JTextField();
		txtBuscarNombre.setBounds(229, 40, 150, 20);
		tbpnBuscarNombre.add(txtBuscarNombre);
		txtBuscarNombre.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.setMnemonic('B');
		btnBuscar.setActionCommand("BUSCAR_NOMBRE");
		btnBuscar.addActionListener(evento);
		btnBuscar.setBounds(229, 96, 89, 23);
		tbpnBuscarNombre.add(btnBuscar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(111, 96, 89, 23);
		btnCancelar.setActionCommand("CANCELAR_BUSQUEDA");
		btnCancelar.addActionListener(evento);
		tbpnBuscarNombre.add(btnCancelar);
		
		tbpnBuscarApellido = new JPanel();
		tbpnBuscarApellido.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		tbpnBuscarApellido.setLayout(null);
		tbpnBuscarApellido.setToolTipText("Aca podra buscar por apellido de contacto");
		tabbedPane.addTab("Apellido", null, tbpnBuscarApellido, "Aca podra buscar por apellido de contacto");
		
		JLabel lblApellidoABuscar = new JLabel("Apellido a buscar");
		lblApellidoABuscar.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblApellidoABuscar.setBounds(76, 38, 124, 20);
		tbpnBuscarApellido.add(lblApellidoABuscar);
		
		txtBuscarApellido = new JTextField();
		txtBuscarApellido.setColumns(10);
		txtBuscarApellido.setBounds(229, 40, 150, 20);
		tbpnBuscarApellido.add(txtBuscarApellido);
		
		JButton button = new JButton("Cancelar");
		button.setBounds(111, 96, 89, 23);
		button.setActionCommand("CANCELAR_BUSQUEDA");
		button.addActionListener(evento);
		tbpnBuscarApellido.add(button);
		
		JButton button_1 = new JButton("Buscar");
		button_1.setMnemonic('B');
		button_1.setActionCommand("BUSCAR_APELLIDO");
		button_1.addActionListener(evento);
		button_1.setBounds(229, 96, 89, 23);
		tbpnBuscarApellido.add(button_1);
		
		tbpnBuscarCiudad = new JPanel();
		tbpnBuscarCiudad.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		tbpnBuscarCiudad.setLayout(null);
		tbpnBuscarCiudad.setToolTipText("Aca podra buscar por ciudad de contacto");
		tabbedPane.addTab("Ciudad", null, tbpnBuscarCiudad, "Aca podra buscar por ciudad de contacto");
		
		JLabel lblCiudadABuscar = new JLabel("Ciudad a buscar");
		lblCiudadABuscar.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCiudadABuscar.setBounds(76, 38, 124, 20);
		tbpnBuscarCiudad.add(lblCiudadABuscar);
		
		txtBuscarCiudad = new JTextField();
		txtBuscarCiudad.setColumns(10);
		txtBuscarCiudad.setBounds(229, 40, 150, 20);
		tbpnBuscarCiudad.add(txtBuscarCiudad);
		
		JButton button_2 = new JButton("Cancelar");
		button_2.setBounds(111, 96, 89, 23);
		button_2.setActionCommand("CANCELAR_BUSQUEDA");
		button_2.addActionListener(evento);
		tbpnBuscarCiudad.add(button_2);
		
		JButton button_3 = new JButton("Buscar");
		button_3.setMnemonic('B');
		button_3.setActionCommand("BUSCAR_CIUDAD");
		button_3.addActionListener(evento);
		button_3.setBounds(229, 96, 89, 23);
		tbpnBuscarCiudad.add(button_3);
	}

	/**
	 * @return the txtBuscarNombre
	 */
	public JTextField getTxtBuscarNombre() {
		return txtBuscarNombre;
	}

	/**
	 * @param txtBuscarNombre the txtBuscarNombre to set
	 */
	public void setTxtBuscarNombre(String txtBuscarNombre) {
		this.txtBuscarNombre.setText(txtBuscarNombre);
	}

	/**
	 * @return the txtBuscarApellido
	 */
	public JTextField getTxtBuscarApellido() {
		return txtBuscarApellido;
	}

	/**
	 * @param txtBuscarApellido the txtBuscarApellido to set
	 */
	public void setTxtBuscarApellido(String txtBuscarApellido) {
		this.txtBuscarApellido.setText(txtBuscarApellido);
	}

	/**
	 * @return the txtBuscarCiudad
	 */
	public JTextField getTxtBuscarCiudad() {
		return txtBuscarCiudad;
	}

	/**
	 * @param txtBuscarCiudad the txtBuscarCiudad to set
	 */
	public void setTxtBuscarCiudad(String txtBuscarCiudad) {
		this.txtBuscarCiudad.setText(txtBuscarCiudad);
	}
	
	
}
