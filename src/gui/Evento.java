/**
 * 
 */
package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Andres
 *
 */
public class Evento implements ActionListener{

	private PrincipalBancoContactos p;
	/**
	 * 
	 */
	public Evento(PrincipalBancoContactos p) {
		// TODO Auto-generated constructor stub
		this.p=p;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String comand=e.getActionCommand();
		if (comand.equals("ACEPTAR_INGRESAR")) {
			p.ingresarPersona();
		}
		if (comand.equals("CANCELAR_INGRESAR")) {
			p.ingresarCeros();
		}
		if (comand.equals("SALIR_GENERAL")) {
			p.salirGeneral();
		}
		if (comand.equals("INGRESAR")||comand.equals("CANCELAR_BUSQUEDA")) {
			p.mostarPnlIngresar();
		}
		if (comand.equals("BUSCAR")) {
			p.crearPnlBuscar();
		}
		if (comand.equals("BUSCAR_NOMBRE")) {
			p.mostrarResultadoBusqueda(0);
		}
		if (comand.equals("BUSCAR_APELLIDO")) {
			p.mostrarResultadoBusqueda(1);
		}
		if (comand.equals("BUSCAR_CIUDAD")) {
			p.mostrarResultadoBusqueda(2);
		}
		if (comand.equals("OK")) {
			p.btnOK();
			p.volverCeros();
		}
		System.out.println(comand);
	}

}
