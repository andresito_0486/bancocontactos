package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;

public class PnlIngresar extends JPanel {
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtTelefono;
	private JTextField txtDireccion;
	
	private Evento evento;
	private JTextField txtCiudad;

	/**
	 * Create the panel.
	 */
	public PnlIngresar(Evento e) {

		setLayout(null);
		this.evento=e;
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNombre.setBounds(93, 41, 80, 20);
		add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblApellido.setBounds(93, 72, 80, 20);
		add(lblApellido);
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono");
		lblTelfono.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTelfono.setBounds(93, 103, 80, 20);
		add(lblTelfono);
		
		JLabel lblDireccion = new JLabel("DIreccion");
		lblDireccion.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDireccion.setBounds(93, 134, 80, 20);
		add(lblDireccion);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(183, 41, 176, 20);
		add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		txtApellido.setBounds(183, 72, 176, 20);
		add(txtApellido);
		
		txtTelefono = new JTextField(new Integer(0));
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(183, 103, 176, 20);
		add(txtTelefono);
		
		txtDireccion = new JTextField();
		txtDireccion.setColumns(10);
		txtDireccion.setBounds(183, 134, 176, 20);
		add(txtDireccion);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(93, 255, 117, 23);
		btnCancelar.setActionCommand("CANCELAR_INGRESAR");
		btnCancelar.addActionListener(evento);
		add(btnCancelar);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setMnemonic('C');
		btnAceptar.setActionCommand("ACEPTAR_INGRESAR");
		btnAceptar.addActionListener(evento);
		btnAceptar.setBounds(242, 255, 117, 23);
		add(btnAceptar);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 223, 430, 2);
		add(separator);
		
		JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCiudad.setBounds(93, 165, 80, 20);
		add(lblCiudad);
		
		txtCiudad = new JTextField();
		txtCiudad.setColumns(10);
		txtCiudad.setBounds(183, 165, 176, 20);
		add(txtCiudad);
	}
	
	/**
	 * metodos de la clase
	 */

	/**
	 * @return the txtNombre
	 */
	public JTextField getTxtNombre() {
		return txtNombre;
	}

	/**
	 * @param txtNombre the txtNombre to set
	 */
	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	/**
	 * @return the txtApellido
	 */
	public JTextField getTxtApellido() {
		return txtApellido;
	}

	/**
	 * @param txtApellido the txtApellido to set
	 */
	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}

	/**
	 * @return the txtTelefono
	 */
	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	/**
	 * @param txtTelefono the txtTelefono to set
	 */
	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	/**
	 * @return the txtDireccion
	 */
	public JTextField getTxtDireccion() {
		return txtDireccion;
	}

	/**
	 * @param txtDireccion the txtDireccion to set
	 */
	public void setTxtDireccion(JTextField txtDireccion) {
		this.txtDireccion = txtDireccion;
	}

	/**
	 * @return the txtCiudad
	 */
	public JTextField getTxtCiudad() {
		return txtCiudad;
	}

	/**
	 * @param txtCiudad the txtCiudad to set
	 */
	public void setTxtCiudad(JTextField txtCiudad) {
		this.txtCiudad = txtCiudad;
	}
	
	
}
