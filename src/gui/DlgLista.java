package gui;

import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;

import logica.Persona;
import logica.bancoContactos;

public class DlgLista extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JList list ;
	private JList listApellido ;
	private JList listTelefono;
	private JList listDIreccion ;
	private JList listCiudad;
	private Evento evento;



	/**
	 * Create the dialog.
	 */
	public DlgLista(Evento e) {
		setBounds(100, 100, 450, 302);
		getContentPane().setLayout(new BorderLayout());
		setResizable(false);
		contentPanel.setLayout(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);


		this.evento=e;

		list = new JList();
		JScrollPane barra=new JScrollPane(list);
		barra.setBounds(10, 11, 420, 200);
//		barra.setLayout(new FlowLayout(5, 5, 5));
		contentPanel.add(barra);

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Aceptar");
				okButton.setActionCommand("OK");
				okButton.addActionListener(evento);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}

		}
	}

	public void actualizarLista(ArrayList lista){
		list.setListData(lista.toArray());
	}

	/**
	 * Launch the application.
//	 */
	//	public static void main(String[] args) {
	//		try {
	//			DlgLista dialog = new DlgLista();
	//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	//			dialog.setVisible(true);
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}

}
