package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import persistencia.Archivo;

import java.awt.CardLayout;
import java.io.IOException;
import java.util.ArrayList;

import logica.Persona;
import logica.bancoContactos;

public class PrincipalBancoContactos extends JFrame {

	private Evento evento;

	private JPanel contentPane;
	private PnlIngresar pnlIngresar;
	private pnlBuscar pnlBuscar;
	private DlgLista dlgLista;
	private Archivo archivo;

	private bancoContactos bancoContactos;

	private CardLayout cardCambio;
	private JMenuBar menuBar; 
	private JMenu mnArchivo ;
	private JMenuItem mntmIngresar;
	private JMenuItem mntmBuscar;
	private JMenuItem mntmSalir ;

	/**
	 * Create the frame.
	 */
	public PrincipalBancoContactos() {
		setTitle("Banco de Datos");
		setBounds(100, 100, 501, 378);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);

		this.evento=new Evento(this);
		
		archivo=new Archivo();
		if (archivo.getFile().length()>0) {
			bancoContactos=(bancoContactos)archivo.load();
		}else{
			bancoContactos=new bancoContactos();
		}
				
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnArchivo = new JMenu("Archivo");
		mnArchivo.setMnemonic('A');
		menuBar.add(mnArchivo);

		mntmIngresar = new JMenuItem("Ingresar");
		mntmIngresar.setMnemonic('I');
		mntmIngresar.setActionCommand("INGRESAR");
		mntmIngresar.addActionListener(evento);
		mnArchivo.add(mntmIngresar);

		mntmBuscar = new JMenuItem("Buscar");
		mntmBuscar.setMnemonic('B');
		mntmBuscar.setActionCommand("BUSCAR");
		mntmBuscar.addActionListener(evento);
		mnArchivo.add(mntmBuscar);

		mntmSalir = new JMenuItem("Salir");
		mntmSalir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, InputEvent.CTRL_MASK));
		mntmSalir.setActionCommand("SALIR_GENERAL");
		mntmSalir.addActionListener(evento);
		mnArchivo.add(mntmSalir);

		cardCambio=new CardLayout();

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(cardCambio);

		pnlIngresar=new PnlIngresar(evento);
		pnlIngresar.setBorder(new TitledBorder("Ingresar Contacto"));
		contentPane.add(pnlIngresar,"card0");
		cardCambio.show(contentPane, "card0");

	}

	/**
	 * Metodos
	 */
	public void ingresarCeros(){
		pnlIngresar.getTxtNombre().setText("");
		pnlIngresar.getTxtApellido().setText("");
		pnlIngresar.getTxtTelefono().setText("");
		pnlIngresar.getTxtDireccion().setText("");
		pnlIngresar.getTxtCiudad().setText("");
	}

	public void ingresarPersona(){
		String nombre=pnlIngresar.getTxtNombre().getText().toUpperCase();
		String apellido=pnlIngresar.getTxtApellido().getText().toUpperCase();
		String telefono=pnlIngresar.getTxtTelefono().getText().toUpperCase().replace(",", "");
		String direccion=pnlIngresar.getTxtDireccion().getText().toUpperCase();
		String ciudad=pnlIngresar.getTxtCiudad().getText().toUpperCase();
		if (nombre.equals(null)||apellido.equals(null)||telefono.equals(null)||direccion.equals(null)||ciudad.equals(null)) {
			JOptionPane.showMessageDialog(this,"El formulario no se ha diligenciado correctamente \n revise y vuelva a intentar", "Error - Diligenciamiento del formato",JOptionPane.ERROR_MESSAGE);
		}else{
			try {
				bancoContactos.setListaPersonas(nombre, apellido, Long.parseLong(telefono), direccion, ciudad);
				JOptionPane.showMessageDialog(this,nombre+" "+apellido+" ha sido agregado correctamente","Confirmacion",JOptionPane.INFORMATION_MESSAGE);
				ingresarCeros();
				archivo.save(bancoContactos);
			} catch (NumberFormatException e) {
				// TODO: handle exception
				JOptionPane.showMessageDialog(this,"Porfavor en el campo designado para el tel�fono esta ingresando valores que no son numericos \n revise y vuelva a intentar", "Error - Diligenciamiento del formato",JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public void crearPnlBuscar(){
		pnlBuscar=new pnlBuscar(evento);
		pnlBuscar.setBorder(new TitledBorder("Buscar Por"));
		contentPane.add(pnlBuscar,"card1");
		cardCambio.show(contentPane, "card1");
	}

	public void mostarPnlIngresar(){
		cardCambio.show(contentPane, "card0");
	}

	public void salirGeneral(){
		this.dispose();
	}

	public void buscarPersona(int i){
		String dato="";
		switch (i) {
		case 0:
			dato=pnlBuscar.getTxtBuscarNombre().getText().toUpperCase();
			if (dato.equals("")) {
				JOptionPane.showMessageDialog(pnlBuscar,"Porfavor ingrese el nombre del contacto que quiere buscar", "Error",JOptionPane.ERROR_MESSAGE);
			}else{
				bancoContactos.buscarPersona(i, dato);
			}
			break;
		case 1:
			dato=pnlBuscar.getTxtBuscarApellido().getText().toUpperCase();
			if (dato.equals("")) {
				JOptionPane.showMessageDialog(pnlBuscar,"Porfavor ingrese el apellido del contacto que quiere buscar", "Error",JOptionPane.ERROR_MESSAGE);
			}else{
				bancoContactos.buscarPersona(i, dato);
			}
			break;
		case 2:
			dato=pnlBuscar.getTxtBuscarCiudad().getText().toUpperCase();
			if (dato.equals("")) {
				JOptionPane.showMessageDialog(pnlBuscar,"Porfavor ingrese la ciudad a la que pertenece el contacto que quiere buscar", "Error",JOptionPane.ERROR_MESSAGE);
			}else{
				bancoContactos.buscarPersona(i, dato);
			}
			break;
		}
	}

	public void btnOK(){
		dlgLista.dispose();
	}


	public void mostrarResultadoBusqueda(int i){
		buscarPersona(i);
		dlgLista=new DlgLista(evento);		
		dlgLista.actualizarLista(bancoContactos.getListaBusqueda());
		dlgLista.setLocationRelativeTo(this);
		dlgLista.setVisible(true);
		dlgLista.setModal(true);		
	}

	public void volverCeros(){
		pnlBuscar.setTxtBuscarApellido("");
		pnlBuscar.setTxtBuscarCiudad("");
		pnlBuscar.setTxtBuscarNombre("");
	}


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalBancoContactos frame = new PrincipalBancoContactos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
