package persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Archivo {
	
	public static final String EXT=".cb";
	public static final String ruta="datos/datos"+EXT;
	public File file=new File(ruta);
	
	
	public void save(Object dato){
		try {
			ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream(new File(ruta)));
			out.writeObject(dato);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	public Object load(){
		try {
			Object dato=null;
			ObjectInputStream in=new ObjectInputStream(new FileInputStream(new File(ruta)));
			dato=in.readObject();
			in.close();
			return dato;
		} catch (Exception e) {
			e.printStackTrace();
		} return null;
	}
	
	public File getFile() {
		return file;
	}
}
