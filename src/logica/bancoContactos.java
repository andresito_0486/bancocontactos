/**
 * 
 */
package logica;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Andres
 *
 */
public class bancoContactos implements Serializable{

	private ArrayList<Persona> listaPersonas;
	private ArrayList listaBusqueda;

	/**
	 * CONSTRUCTOR DE LA LISTA DE PERSONAS SIN PARAMETROS PARA
	 * LA VERSION BASICA
	 */
	public bancoContactos() {
		listaPersonas=new ArrayList<Persona>();
		// TODO Auto-generated constructor stub
	}


	/**
	 * METODOS
	 */

	/**
	 * @return the listaPersonas
	 */
	public ArrayList<Persona> getListaPersonas() {
		return listaPersonas;
	}

	/**
	 * @param persona persona que va a ingresar al banco de contactos
	 */
	public void setListaPersonas(String nombre, String apellido, long telefono,
			String direccion, String ciudad) {
		Persona p=new Persona(nombre, apellido, telefono, direccion, ciudad);
		listaPersonas.add(p);
	}

	/**
	 * busqueda de personas ya se por nombre o por ciudad
	 * @param pos
	 * @param dato
	 */
	public void buscarPersona(int pos,String dato){
		listaBusqueda=new ArrayList();
		int a=0;
		String cadena="";
		while (a<listaPersonas.size()&&listaBusqueda.contains(dato)!=true) {
			if (listaBusqueda.contains(cadena)) {
				a++;
			}else{
				switch (pos) {
				case 0:
					for (int i = 0; i < listaPersonas.size(); i++) {
						if (listaPersonas.get(i).getNombre().contains(dato)) {
							cadena="Nombre: "+listaPersonas.get(i).getNombre()+" "+listaPersonas.get(i).getApellido()+" - Tel�fono: "+listaPersonas.get(i).getTelefono()+" - Direcci�n: "+listaPersonas.get(i).getDireccion()+" - Ciudad: "+listaPersonas.get(i).getCiudad();
							listaBusqueda.add(cadena);
						}
					}
					a++;
					break;

				case 1:
					for (int i = 0; i < listaPersonas.size(); i++) {
						if (listaPersonas.get(i).getApellido().contains(dato)) {
							cadena="Nombre: "+listaPersonas.get(i).getNombre()+" "+listaPersonas.get(i).getApellido()+" - Tel�fono: "+listaPersonas.get(i).getTelefono()+" - Direcci�n: "+listaPersonas.get(i).getDireccion()+" - Ciudad: "+listaPersonas.get(i).getCiudad();
									listaBusqueda.add(cadena);
						}
					}
					a++;
					break;

				case 2:
					for (int i = 0; i < listaPersonas.size(); i++) {
						if (listaPersonas.get(i).getCiudad().contains(dato)) {
							cadena="Nombre: "+listaPersonas.get(i).getNombre()+" "+listaPersonas.get(i).getApellido()+" - Tel�fono: "+listaPersonas.get(i).getTelefono()+" - Direcci�n: "+listaPersonas.get(i).getDireccion()+" - Ciudad: "+listaPersonas.get(i).getCiudad();
									listaBusqueda.add(cadena);
						}
					}
					a++;
					break;
				}
			}
		}
	}




	public ArrayList getListaBusqueda() {
		for (int i = 0; i < listaBusqueda.size(); i++) {
			System.out.println(listaBusqueda.get(i));
		}
		return listaBusqueda;
	}
	
	public void setListaPersonas(ArrayList<Persona> listaPersonas){
		this.listaPersonas=listaPersonas;
	}

}
