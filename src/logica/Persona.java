/**
 * 
 */
package logica;

import java.io.Serializable;

/**
 * @author Andres
 *
 */
public class Persona implements Serializable{
	
	/**
	 * nombre de persona
	 */
	private String nombre;
	/**
	 * apellido de la persona
	 */
	private String apellido;
	/**
	 * telefono de la persona
	 */
	private long telefono;
	/**
	 * direccion de la persona
	 */
	private String direccion;
	/**
	 * ciudad de residencia
	 */
	private String ciudad;

	/**
	 * CONSTRUCTOR DE LA CLASE SIN PARAMETROS PARA LA VERSION BASICA
	 */
	public Persona() {
		nombre="";
		apellido="";
		telefono=0;
		direccion="";
		ciudad="";
		// TODO Auto-generated constructor stub
	}

	/**
	 * CONTRCUTOR DE LA CLASE CON PARAMETROS PARA LA VERSION BASICA
	 * @param nombre NOMBRE DE LA PERSONA
	 * @param apellido APELLIDO DE LA PERSONA
	 * @param telefono TELEFONO DE LA PERSONA
	 * @param direccion DIRECCION DE LA PERSONA
	 */
	public Persona(String nombre, String apellido, long telefono,
			String direccion, String ciudad) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.direccion = direccion;
		this.ciudad=ciudad;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * @return the telefono
	 */
	public long getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCiudad() {
		return ciudad;
	}
	
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
}
